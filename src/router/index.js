import Vue from 'vue'
import VueRouter from 'vue-router'
import TableA from '../views/TableA.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'TableA',
    component: TableA
  },
  {
    path: '/table-b',
    name: 'TableB',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/TableB.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router

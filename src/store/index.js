import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { baseURL } from '../config'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    numbers: [],
    quantity: [],
    position: [],
    paragraph: [],
    letters: [
      'A',
      'B',
      'C',
      'D',
      'E',
      'F',
      'G',
      'H',
      'I',
      'J',
      'K',
      'L',
      'M',
      'N',
      'O',
      'P',
      'Q',
      'R',
      'S',
      'T',
      'U',
      'V',
      'X',
      'W',
      'Y',
      'Z'
    ],
    loading: true,
    error: false,
    message: ''
  },
  mutations: {
    errorHandler(state, payload) {
      state.loading = false,
      state.error = true
      state.message = payload
    },
    setNumbers(state, payload) {
      // ordenamiento con metodo burbuja
      function bubbleSort(vector, i) {
        let j = 0
        while((j <= i) && (vector[i - j] < vector[i - j - 1])){
          let aux = vector[i-j-1]
          vector[i - j - 1] = vector[i - j]
          vector[i - j] = aux
          j++
        }
      }
      // ordenamiendo con metodo gnomo
      function gnomeSort(vector) {
        for (let i = 0; i < vector.length; i++) {
          bubbleSort(vector, i)
        }
      }
      // busca las ocurrencias de un numero
      function searchOccurrences(vector) {
        vector.map((i, index) => {
          let indexA = vector[index]
          let indexB = vector[index + 1]

          if(indexB != indexA) {
            state.quantity.push(vector.filter((j) => j == i))
          }
        })
      }
      // busca la(s) posicion(es) de un numero
      function searchPosition() {
        let vector = JSON.parse(JSON.stringify(payload))

        state.quantity.map((i) => {
          let element = i[0];
          let idx = vector.indexOf(element);

          while (idx != -1) {
            state.position.push({
              number: element,
              position: idx
            })
            idx = vector.indexOf(element, idx + 1);
          }
        })
      }
      // se obtiene el objeto final
      function setData() {
        let vector = JSON.parse(JSON.stringify(state.position))
        let invertedVector = JSON.parse(JSON.stringify(state.position.reverse()))

        state.quantity.map((i, index) => {
          data[index] = {
            index: index,
            number: i[0],
            quantity: i.length,
            position: {
              first: vector.find((element) => element.number == i[0]),
              last: invertedVector.find((element) => element.number == i[0])
            }
          }
        })
      }

      const vector = JSON.parse(JSON.stringify(payload))
      const data = []

      gnomeSort(vector)
      searchOccurrences(vector)
      searchPosition()
      setData()

      state.numbers = data
      state.loading = false
    },
    setParagraph(state, payload) {
      // lee cada uno de los paragraph
      function readStrings() {
        vector.map((i) => {
          state.letters.map((l) => {
            aux.push({
              letters: l,
              quantity: searchOccurrences(i.paragraph, l)
            })

            if (l.toUpperCase() == 'Z') {
              state.paragraph.push(aux)
              aux = []
            }
          })
        })
      }
      // cuenta la candidad de repeticiones por letra
      function searchOccurrences(string, characters){
        var index = [];

        for(var i = 0; i < string.length; i++) {
          if (string[i].toUpperCase() == characters){
            index.push(i);
          }
        }
        return index.length;
      }

      const vector = JSON.parse(payload)
      var aux = []

      readStrings()
      state.loading = false
    },
    setReset(state, payload) {

      state.loading = true,
      state.error = false,
      state.message = ''

      if (payload == 'numbers') {
        state.numbers = []
        state.quantity = []
        state.position = []        
      } else {
        state.paragraph = []
      }
    }
  },
  actions: {
    getNumbers({ commit }) {
      return axios({
        method: 'get',
        url: `${baseURL}/array.php`,
        headers: {
          'Content-Type': 'application/json',
        },
      }).then((result) => {
        commit('setNumbers', result.data.data)
      }).catch((err) => {
        commit('errorHandler', err)
        console.log('error in getNumbers', err)
      })
    },
    getParagraph({ commit }) {
      return axios({
        method: 'get',
        url: `${baseURL}/dict.php`,
        headers: {
          'Content-Type': 'application/json',
        },
      }).then((result) => {
        commit('setParagraph', result.data.data)
      }).catch((err) => {
        commit('errorHandler', err)
        console.log('error in getParagraph', err)
      })
    },
  }
})